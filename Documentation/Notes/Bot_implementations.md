# Bot implementations

Content creators can craft bots for the game, by providing code files governing the behaviour of the AI.

AIs have names, descriptions, and associated code. Optionally they can have images, sounds, and reaction text, as well as attribute restrictions (they will only use heroes with given attributes).
