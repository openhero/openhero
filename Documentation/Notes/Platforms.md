# Platforms

The target is to be multi-platform, so that OpenHero-based games can be played by anyone, anywhere. The ideal is for it to be runnable on Windows, Linux and Android; iOS and macOS are secondary considerations, due to the build tooling licensing, therefore someone else will need to do that porting work.

The interface should aim to be lightweight, so that low-spec computers can run the game, and so that it can be run on entry-level mobile devices as well.

## Interface

Unifying the interface paradigm across all platforms is preferable. Click-based interaction is to be preferred.

From the requirement of being lightweight, the design and "flavour" of the environment will need to be 2D-oriented, keeping unnecessary animation loops out, at least for the initial implementation. 3D elements are prohibited, renderers must always be optional.
