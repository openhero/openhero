# Gameplay

This document sets out the overall design of intended gameplay for the core features of the game.

## Mechanics

The mechanics are intended to be as generalised as possible, to allow for extensibility. If a mechanic is not generalizable, question its viability for the engine.

## Unit Management

A Player chooses ("recruits") 4 Combat Units from a roster of Units that is unique to the Player into a Battle Party. They can then battle an AI or another Player, each using their 4-Unit combination of Units.

Any Unit in a fight is deemed "In Play." A Unit is not in-play when it is neither on the Front Line, or in the Trench. Combat is turn-based, and each Unit can perform 1 action when it is its turn, the action being defined by the controlling Player.

The Player chooses an ally Unit they control, to attack an enemy Unit. Defeat occurs when they have no more Units on the Front Line (see below, Positions)

### Unit acquisition

By fighting battles, Players earn Ether Points, 1 per battle won. A Player can spend Ether points to cash into a "gatcha" roulette to obtain a new Unit, chosen from the global Unit database, with results controlled rarity ratings.

When a Player creates a game profile, they are given 8 Units to start with. Each are chosen from the game's total Units running the gatcha once per Unit awarded.

Players can permanently remove any one Unit from their roster to receive 2 Ether Points.

Rosters are not allowed to have fewer than 8 Units.

### Unit Upgrade

During Battles, Units can receive 1 Skill Point for each enemy Unit defeated by that allied Unit. If a Unit dies in Battle, they retain Skill Points already earned during that Battle.

Units are acquired with the standard available Abilities, and up to 1 custom Ability. Players can unlock up to 3 additional Abilities, in a predetermined order for that Unit, by using Skill Points. The cost for unlocking these actions depends on the definition of the Unit. The default suggested costings are 10, 25 and 60 Skill Points for each of the consecutive Abilities.

### Unit Leveling

Units start at Level 1, and can progress to a maximum of Level 20. To Level Up to level `L` from level `L-1`, Skill Points must be used at a cost of `X` where `X = 2 * L`

Each of their Basic Ability stats has a growth ratio of N (for base stat value at Level 1, their levelled stat value at level `L` is `LevelledStat(L) = BASE_VALUE + floor(N/10)*L`). N is defined on `0 < N < 20`

Each additional Ability requires the character to achieve levels 5, 10 and 15 respectively. Additional Abilities do not change with levels.

## Combat

### Determining turn order

Combat is turn-based. A value called "Turn Order Value" (TOV) is associated with every Unit, and any action a Unit can take also has a TOV.

Units receive priority in a given order determined by their base TOV value - a smaller TOV means they play sooner. At start of play, all Units are sorted in ascending order by TOV. If a set of Units has equal TOV, they are then ordered by attack (ascending), defense (descending), and then finally shuffled (for example, multiple identical Units).

To determine the next attacker, the smallest TOV value of all Units in-play is removed from each Unit. The Unit whose TOV was smallest is now at zero, and can take an action. Once they have taken an action, their TOV is set to the value associated with their action. If the value would give them an equal TOV value of another Unit, their TOV value is incremented by 1 as necessary until their TOV no longer is equal to an in-play Unit.

#### Turn Order Chain Bonus

After a Unit uses a standard Attack Ability (with or without Armament - advanced Abilities on Armaments do not count), if the next combat action is a standard Attack from a Unit from the same team, the second Unit's attack receives an Attack stat boost. Third and fourth attackers from the same team without interruption in the chain receive further Attack stat boosts. After the fourth Attack, the stat boost level is maintained, but does not further grow.

The chain is broken if after an Attack is performed, the next Action in combat is not a basic Attack from a member of the same team. Trench Abilities interrupt the chain.

### Positions

There are two positions Units can occupy on the battlefield: Front Line, and Trench. All Units start on the Front Line. On any of their own turns, before instructing a Unit to take an action, the Player can move any number of Units to the Trench - but they cannot send a Unit from the Trench back to the Front Line. Some Abilities may force a Unit back onto the Front Line

Defeat occurs when either a Player loses all their Units, or moves all their remaining Units to the Trench.

Trench Abilities can provide support, but mean that the remaining Front Line Units bear the brunt of enemy attacks.

## Standard Actions

Every Unit has a set of standard Actions:

* Basic Action - labelled Attack, Heal or an Ability name, depending on Unit and equipped Armament
* Guard
* Fall Back
* Throw
* Use Ability

Every Unit has a set of standard stats:

* HP
* Might - affecting their Attack, Healing, etc
* Protection - subtracted from the Attack value it is receiving
* Nature - whether they are magical or physical
* TOV

### Attacking

A Player's Unit can deal damage to an enemy Unit equal to the value of its Might.

If the Unit is equipped with an Armament, the Attack value of the Armament is summed with the Unit's Might value, or changes the Nature of their Attack, and/or changes their Basic Action to an Advanced Ability.

Other conditions may modify the Attack value further.

If an Armament changes the Basic Action of the Unit, the Unit can no longer use their Basic Action until the Armament is thrown or unequipped.

### Guarding

If a Unit uses Guard, it will take only half damage from any hit on it. Guarding automatically has a TOV of half the Unit's base TOV.

### Falling Back

At the start of a turn, a Player can select any Units on the Front Line to make them Fall Back.

If a Player chooses to make a Unit Fall Back, the Unit is placed in the Trench. Fallen Back Units are removed from the TOV sequence, and can no longer be the target of Basic Attacks. Their Trench-based abilities still may affect their Front Line allies, or Front Line enemies.

When a Front Line Unit's turn comes, the Player may use the Trench Ability of at most one Trench Unit before the Front Line Unit takes action.

### Throwing

If a Front Line Unit is equipped with an Armament, that armament can be Thrown for its Throw Ability effect, determined by the Armament. That Armament is then lost to the Player, and must be re-acquired.

Throwing cannot be performed as a Trench Action.

### Using an Ability

An Ability is an Action that is not one of the standard Actions. Typically Abilites are provided by an Ability Keyword, parametrized by a numerical value.

## Armaments

A Unit can be equipped with an Armament during team selection in the Roster. Armaments can be of Physical or Magical nature.

A Unit has a base Might, Nature and Basic Action equivalent of an Armament. If a Unit is equipped with an Armament:

* their Might is summed with the Armament's Might, if specified
* the Nature of the Armament, if specified, takes precedence over the Unit's Nature
* the Action type of the Armament, if specified, overrides the Unit's Basic Action type
* the Throw Ability of an armament determines its effect when thrown - if not specified, the Ability is simply "Attack"
* the Throw Value of an armament determines the potency of its effect - if not specified, the value is 75% of the Might of the Unit
