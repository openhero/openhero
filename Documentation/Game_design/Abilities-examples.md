# Abilities and Armaments Examples

The following are some example Attributes and Abilities, as a study on how useful the idea of Abilities is, and Armaments they could also be attached to.

Some Attributes and Abilities can be configured with `N` values, some can be configured with a list of strings, `List`.

`N` is the configurable value of the Attribute as stated on the Unit or Armament.

The strings serve to further configure the effect handler.

Where the same Attribute is on an Armament or Unit or on a Unit's Modifier Stack, they all contribute to affecting the base stat, but are each calculated from the levelled, but otherwise unmodified base stat:

* Modifier with the most turns remaining
    * If equal modifiers found, the one with the highest effect takes precedence
* Armament's Attribute
* Unit's Attribute

So if a Unit is healed by 10 HP and has "Improves healing received by 10%" with a "Improves healing received by 5", this sums to an overall being healed for 10% of 10 (=1) and an additional 5, for 6HP - and never "10% of 15" !



## Attributes

Attributes are passively active on Units and can be innate to the Unit, added from an Armament, or temporarily added by a Buff/Debuff Action.

Some Attributes simply "exist" as informational properties on a Unit, which affect Abilities.

### Universal Receiver

Unit gains `N` more HP from healing Actions.

### Tracker

Unit's next Action has the same TOV cost of the target of the Action.

### Coward

Dodges Party-wide attacks with `N%` success

### Dodger

Dodges Party-wide attacks at the cost of adding `N` TOV to their placement

### Riposte

When attacked, deals `N%` of Might as damage to the Unit that attacked it.

### Survivor

Survives any fatal attack with 1 HP at `N%` chance

### Bloody Nightmare

When HP under 10%, Might is increased by `N%` 

### Hang In There!

Unit always has at least a temporary 1 HP. Abilities and attacks that reduce the Unit's HP to 0 do so, but this Attribute layers on top of the normal HP

(Note - this would be from a Buff - it times out after Unit's turn, could be good in a pinch if you think you can get your healer to act fast!)

### Poisoned

At start of turn, does 3 damage to Unit. Active for `N` turns, or permanently if not specified.

(Note - This could be a debuff. it could also be a curse on an Armament)

### Hardy

Reduces damage to Unit by `N`

### Fortress

Reduces damage to Unit by `N%`

### Mighty

Increases Might by `N%`

### Defensive

Increases Protection by `N%`

(Note - Defensive and Fortress affect different stats)



## Abilities

Abilities are alternative Actions a Unit can perform.

### Standard Actions

The standard Actions are are simple Abilities themselves

* Attack - damage target foe for `N`
* Guard - reduce damage on Unit by 50%
* Heal - Heal for `N`

### Body Slam

Unit deals `N%` more damage than attack base. Unit receives `(N/5)%` of total damage dealt to foe as damage to itself.

### Heal Fighters

Heals all Front Line Units' HP for `N%` of the Unit's Might

### Heal All

Heals all Front Line and Trench Units for `N` HP

### Shouting Match

Add `N` TOV to target foe's placement

### Scream Like A Banshee

Add `N` TOV to each Front Line foes' placements

### Egg On

Reduce target ally's TOV placement for `N`, increase Unit's TOV placement for `N`

### Swap

Swap TOV placement with target ally

### Hunger

Damage foe for `N`, gain `N/2` HP

### Defend

Unit is attacked instead of defended ally until Unit's next turn. Attributes triggered when Unit is attacked trigger here too.

### Tantrum

Takes all Front Line allies' equipment, and throws each at random targets for 150% of effect value. Throws Throws Buff and Heal items at allies `N%` of time, throws Buff items at foes `(100-N)%` of time, vice versa for Debuff, Defenses and Weapons. N must be a value from 10 to 90.

### Yoink!

Unit takes the Armament of an ally Unit, for `N%` of the TOV of the Armament.

### Call in a Favour

Take `N` HP from allied Unit and heal Unit for `N` HP.

### Share Burden

Take `N` HP damage and heal allied Unit for `N` HP.

### Smite

Damages target foe for `N%` of Might

### Martyr

Deal damage to enemy Front Line foes equal to 200% of Unit's HP. Unit's HP then drops to 0

### Self Sacrifice

Deal damage to enemy Front Line foes equal to 150% of Unit's Might. Unit's HP then drops to 0

### Call Airstrike

Deal `15` damage to all Front Line Units, ally and enemy, for `N%` hit success rate.

### Get Over Here!

Pull target Trench Unit enemy back to the Front Line

### Summon Veteran

Unit's HP drops to 0. If Unit survives, Summon fails; otherwise Summons a random Unit from the roster and places them in the Front Line.

`N%` chance to only pick from the top 4 most characters, sorted by Level

SP gained is awarded to Summoned Unit, not Summoner Unit

(Note - mess with players by putting this on on a character with "Survivor ; N=40" !)

### Cleansing Wave

Clears all buffs and debuffs from ally Front Line Units

### Cleansing Breeze

Clears all debuffs from ally Front Line Units

### Quake

Clears all buffs from enemy Front Line Units

### Hard Reset

Clears all buffs and debuffs from all Front Line Units, allies and enemies.

### Judgement

Deals 20% Unit max HP as damage to Unit, if Unit has an attribute in `List`



## Armamanets

Armaments can add attributes to a Unit, switch their Basic Action to the described Ability, change their Nature to that of the Armament.

### Iron Sword

A very basic Armament which gives a slight increase in Might and causes a Nature of Physical.

* Ability: Attack
* Nature: Physical
* Might: 5

### Magic Staff

Another basic Armament, like the Iron Sword, but causes a Nature of Magical.

* Ability: Attack
* Nature: Magical

### Wizard Robes

Gives a Might stat buff, as well as redeifining the Nature of the Unit. Throwing it has an effect on the Front Line allies, rather than on the enemy

* Attributes: {Mighty = "5%"}
* Nature: Magical
* Throw Action: Heal Fighters
* Throw Value: 5

### Priest Robes

Thematically different, functionally similar to the Wizard Robes

* Ability: Heal
* Nature: Magical
* Throw Action: Heal All
* Throw Value: 5

### Paladin Mace

Assigns a Nature, grants a self-Heal along with the attack.

* Ability: Hunger
* Attributes: Hardy
* Nature: Physical
* Throw Action: Attack
* Throw Value: 20

### Rhino Horn

At first glance a powerful but tricky Armament. Grants improved defense, and an improved attack when HP is low, but also saps HP from Unit when used. Stuns a target rather than damaging them when thrown.

* Ability: Body Slam
* Nature: Physical
* Attributes: Defensive, Bloody Nightmare
* Throw Action: Shouting Match
* Throw Value: 40

### Tome of the Dead

A Magical book of damnation. It can deal damage to any Unit on the Front Lines (as per Judgement's description) that has any of the listed Attributes.

* Ability: Judgement
* Nature: Magical
* List: ["Holy", "Human", "Nature", "Animal"]
* Throw Action: Poisoned
* Throw Value: 4

### Scripture of Prophecy

Thematically opposite to the Tome of the Dead. Still uses Judgement, but heals when thrown. (... wait what)

* Ability: Judgement
* Nature: Magical
* List: ["Holy", "Human", "Nature", "Animal"]
* Throw Action: Heal All
* Throw Value: 5
