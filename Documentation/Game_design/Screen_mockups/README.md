# Mockups

The files in this directory are for layout reference. They are created in LibreOffice Draw from [LibreOffice 6][libreoffice]

[libreoffice]: https://www.libreoffice.org/
