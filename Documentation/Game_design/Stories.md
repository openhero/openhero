# Stories

A Story combines the use of Dialogues and one or more Campaign Maps to form a narrative element to provide a Story Mode.

A Story consists of one or more Campaigns. Each Campaign has one Map, pre-crafted or randomly-generated.

Campaigns carry IDs. Any Campaign can require Map Victory in another Campaign of its own Story.

## Maps

A Map can be played by having the Player move around an `{x,y}`-tiled grid map of paths.

Maps can be supplied as pre-crafted layouts, with fixed Event points, or randomly-generated.

Any tile is candidate for a Battle event, optionally with dialogue. The Map may be configured such that visited tiles generate new battle events. The Map may be configured to show or hide Battle events.

### Map Restrictions

A map may limit the characters allowed to be used to play a map to being from a Character Family set. The Player must have the requisite characters in their Roster to be able to play the Map. If the Player does not have the requisite characters, the game may, prior to loading, offer to give the Player a random selection of characters from the required Character Family set(s), using the same gatcha rules, costing half the EP normally required for the number of characters to summon.

### Map Characters

NTH (nice to have)

A map may have bot characters on it. A character on a map may move to an adjacent tile on a map, randomly.

This calculation can also run during comabt - if there is room for the Unit, and the Unit moves to the tile where combat is taking place, it may join the current fight.

## Replayability

A Player may change their lineup at any time or change Armaments - however they cannot run any non-Map games including general random battles, or perform Summonings. They can abandon a Map at any time, but their Map progress is then lost:

* if the Map was crafted, it is reset for next playthrough.
* if the Map was randomly generated, it is discarded.

After a Campaign is completed by obtaining Map Victory, a Completion Reward can be given to the Player. Upon subsequent replays, the Completion Reward may or may not be different. The Campaign that presented the Map is then marked as completed.

## Dialogues

Pre- and post- battle dialogues can be played on any given Battle.

When a Map Battle is initiated, a pre-battle Dialogue is checked for and played. The player can click through the dialog, or skip it altogether, and proceed to Battle.

If the Player wins the Battle, a post-battle Dialogue is checked for and played.

Dialogues may or may not be accompanied by voice lines.
