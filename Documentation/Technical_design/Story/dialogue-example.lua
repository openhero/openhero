--[[
This is an example of how a dialogue might be directly coded.

Ideally however, dialogue would be loaded from a text specification.
--]]

local mydialogue = DialogueRegistrar()

local c_zeus = "org.myname:zeus"
local c_hera = "org.myname:hera"
local c_herc = "org.myname:hercules"

mydialgue.registerCharacter(c_zeus, "#c33", c_zeus)
mydialgue.registerCharacter(c_hera, "#3c3", c_hera)
mydialgue.registerCharacter(c_herc, "#33c", c_herc)

mydialogue.setBackgroundImage("royal_hall.png")
mydialogue.setBackgroundMusic("epic.ogg")

mydialogue.say({
    characters = DialogueRegistrar.narrator,
    text = "In the Hall of the Gods ..."
})

mydialogue.say({
    characters = {
        {character = c_herc, illustration = "hercules_defiant.png", position = Dialogue.LEFT}
    },
    text = "I have come to defeat you!"
})

mydialogue.say ({
    characters = {
        {character = c_zeus, illustration = "zeus_smug.png", position = Dialogue.RIGHT}
    },
    text = "You're a cocky brat. We shall fight!"
})

mydialogue.setBackgroundImage("fire.png")
mydialogue.setBackgroundMusic("danger.ogg")

mydialogue.say({
    characters = {
        {character = c_hera, illustration = "hera_center_shout.png", position = Dialogue.CENTER}
    },
    text = "WAIT!!"
})

mydialogue.say({
    characters = {
        { character = c_herc, illustration = "hercules_normal.png", position = Dialogue.LEFT},
        { character = c_zeus, illustration = "zeus_normal.png", position = Dialogue.RIGHT}
    },
    text = "!!!"
})

mydialogue.say({
    characters = {
        {character = c_hera, illustration = "hera_center_shout.png", position = Dialogue.CENTER}
    },
    text = "I am your arbiter."
})

mydialogue.say({
    characters = {
        { character = c_herc, illustration = "hercules_normal.png", position = Dialogue.LEFT},
        { character = c_zeus, illustration = "zeus_normal.png", position = Dialogue.LEF}
    },
    text = "Let's do this ..."
})
