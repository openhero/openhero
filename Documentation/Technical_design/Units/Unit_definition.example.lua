--[[
An example definition of a Unit
--]]

local my_unit = {
    id = "org.myname:unit:unit_name",
    oh_version = {maj=0,min=1,patch=0}, -- version of OH assumed when developing this unit
    hp = 20,
    might = 10,
    protection = 5,
    tov = 100,
    level = 1,
    nature = "physical",
    action = "attack", --  or an ability declaration
    modifier_stack = [],
    ability_1 = {name="heal", value=5, tov=80}, -- first unlockable ability,
    ability_2 = {}, -- second, etc
    ability_3 = {},
    ability_4 = {},
    abilities_unlocked = 0, -- how many abilities in the sequence are unlocked
    attributes = {},
    growth_ratios = {
        hp = 3,
        might = 2,
        protection = 1,
        tov = -5
    },
    armament = nil,
    rarity = 10,

    display = { -- aesthetic properties
        name = "Unit Name",
        description = "A valorous unit from the base set.",
        sprite_frames = {
            sprite_map = "sprites_unit_name.png",
            frames_default = { -- Default set of frames, more may be added as an extension
                width = 31, -- Using sprites from a 32x(32*N) map, omit rightmost and bottommost pixels
                height = 31,
                vertical = false -- assumes either a left-to-right set (when false) or top-down set (when true)
            }
        },
        illustrations = {
            "neutral" = "illus_unit_name_neutral.png",
            "attacking" = "illus_unit_name_attacking.png",
            "defending" = "illus_unit_name_defending.png"
        }
    }
}

OpenHero:Unit:register(my_unit)
