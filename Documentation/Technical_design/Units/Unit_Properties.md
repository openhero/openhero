# Unit Properties

This document attempts to set out core technical properties for gameplay with regards to Units.

All Units have a base set of properties that Must be defined.

Additional properties can then be added through "Attributes" and be given meaning by "Attribute Handlers" called during an attack cycle.

## Universal Unit Properties

Each combat unit has the following mandatory Mechanics properties:

* ID - an ID string to uniquely identify the unit definition
* Base HP - the health points the unit starts with at start of play, and the maximum health points they can have at any time
* Might - the damage a unit can deliver when attacking, healing de/buffing, without any modifiers (including buffs, debuffs, items, etc), summed with Armament if equipped
* Protection - the amount of damage nullified when being attacked, summed with Armament if equipped
* Turn Order Value
* Level - the level of the Unit
* Nature - Magical or Physical, superceded by Armament if equipped
* Action - `attack`, or an Ability declaration, superceded by Armament if equipped
* Modifier Stack - a stack of effects, added to by Abilities. After the Unit's turn is over, each modifier's counter is decremented by 1. Upon reaching zero, the stack is cleared
* Ability Level 1 - the entry-level additional Ability the Unit can perform
* Ability Level 2 - the first unlockable Ability (optional)
* Ability Level 3 - the second unlockable Ability (optional)
* Ability Level 4 - the third unlockable Ability (optional)
* Attributes List - a list of attributes attached to the unit
* Level Growth Ratios - a list of attributes regarding the Unit's level
* Armament - a slot for a single Armament
* Rarity - a value from 0 to 10, see Rarity

Additional to these, they have the following mandatory Aesthetic properties:

* Name
* Description
* Sprite Frames Set - the set of right-facing square sprites when on the battlefield
    * Optionally an added "Sprite Frames Set Counterpart" when displayed as an enemy, for non-symmetrical Units
    * (this option is not MVP)
* Illustration Frames \[Neutral,Attacking,Defending\] - larger right-facing square images displayed when a Unit is shown to be in info panel or speaking, performing an action, or being affected by an action
    * Optionally an added "Illustration Frames Counterpart \[Neutral,Attacking,Defending\]" for displaying enemy actions, or non-player-party in a conversation scene
    * (option is not MVP)

### Unit Leveling

Units start at Level 1, and can progress to a maximum of Level 20. To Level Up to level `L` from level `L-1`, Skill Points must be used at a cost of `X` where `X = 2 * L` For example, to progress a Unit from level 15 to level 16, `16 * 2 = 32` Skill Points must be spent. Similarly, to progress to level 17, an additional 34 of the Unit's Skill Points must be spent.

Each of their Basic Ability stats has a growth ratio of N (for base stat value at Level 1, their levelled stat value at level `L` is `LevelledStat(L) = BASE_VALUE + floor(N/10.0)*L`). N is recommended to be in the range of `-10 <= N <= 20`

For example, given a character with defined Growth Ratios, their level 1 and level 20 stats progress in such a fashion:

```
                    HP       Might     Protection    TOV
Growth Ratio        12       15        7             -1

Stats Level 1       20       10        5             30

Stats Level 20      44       40        19            22
```

Each additional Ability requires the character to achieve levels 5, 10 and 15 respectively. Additional Abilities' stats do not change with levels.

## Abilities

Abilities determine something a Unit can do.

An Attribute's name acts as a Keyword mapped to a handler to evaluate its effect.

```
ability_function( unit_user , unit_target , battle )
```

The Unit using an ability as well as the Unit being targeted are supplied, the `battle` object still gives access to the entire Battle field so as to check for additional environmental factors

## Actions

Actions are generic Abilities can either be Attack, Support, Defense or Heal. Every Unit has a basic Action.

* Attack will cause a Unit to attack foes as normal course of action
* Defense uses the Unit's `Protection` to guard an ally when it is attacked
    * Effect lasts until Unit's next turn
* Heal can heal allies' HP
* Support affects other stats, and is associated with a Keyword ability

Each Action a unit can take has the following Mechanics properties:

* Turn Order Value
* Target Type - self, ally or foe
* Stat Value Name
* Stat Value Modifier - the value to apply - see Modifiers
* Modifier Stack - whether the effect uses the stat modification stack, or modifies the stat directly
* Conditions - checks for presence of Attributes, or Attribute value thresholds

Each action also has the following Aesthetic properties:

* Name
* Icon
* Description

## Attributes

Each attribute has the following mandatory Mechanical properties:

* id - a string consisting of the identifier for the attribute
* value - an integer representing... the machanics value of the attribute.

The mandatory aesthetic properties are:

* Name - the name of the attribute for display in the game
* Icon - the square graphical representation of the item
* Description

## Modifiers

Different Abilities can add various Modifiers on a Unit's Modifier Stack.

When checking for a Unit's stat, the Modifiers are combined from the bottom of the stack upwards, before yeilding the calculated stat value. A modifier last for as many turns as specified, before being removed from the stack.

A modifier has the following Mechanical properties:

* Type: `buff` or `debuff`
* Duration: an int representing how many turns it lasts for
* Value: a string defined by `["+" or "-"] INTEGER ["%"]`
    * An int alone sets a value
    * using "+" or "-" with an integer sums with the attribute's current value
    * using "%" with an integer applies the product to the attribute's base value and divides by 100, taking the floor value
    * using "%" in conjunction with either "+" or "-" takes the product of the int and the attribute's base value, and divides by 100, taking the floor value
 together with the Unit's stat

Modifiers also have the following Aesthetic properties:

* Name
* Icon
* Description

## Armaments

Armaments are pieces of equipment that can modify any combination of a Unit's Nature, Action, Range, Might or Protection whilst equipped.

Armaments can only be assigned to Units outside of Battle. Armaments are owned indefinitely. If they are Thrown during Battle, they are lost to the Player until a new instance is acquired.

Armaments are weapons or protective gear. They may be occasionally be dropped by particular enemies, upon defeat, or offered as rewards in Campaigns. They are obtained by the player only on the condition of Battle Victory or Map Victory.

Armaments have the following mandatory Mechanics properties:

* Nature: whether `Physical` or `Magical` - supercedes the Nature of the Unit when equipped
* Action: `Weapon`, `Support` or `Heal` - supercedes the Type ; or `Defense`
* Value: summed with the unit's Might value if `Weapon` or `Heal` type, sets a `Protection` value if `Defense`
* Throw Action: the effect of throwing the Armament
* Throw Value: the value of the item's effect when thrown
* Range: `Single` or `Party` determines how many Front Line enemies are damaged
* Attributes List: the attributes attached to the weapon, combined as a set union with the Attributes of the Unit.

Aesthetic properties:

* Name - the name of the attribute for display in the game
* Icon - the graphical representation of the item, within a 64x64 square
* Projectile - the graphical representation of the Armament for when it is thrown, displayed statically on the enemy Unit's Illustration

# Rarity

This stat is solely used during the "gatcha" summoning mechanism.

See the Summoning notes document.
