# Asset distribution format

Assets should be packaged and distributed as a file hierarchy with the below sections. Each section (Abilities, Equipment, Stories and Units) is optional for a given asset pack

```
+-- "/"
    |
    +-- OpenHero.yaml --> Defines the name of the asset pack and its "Family" identifier
    |
    +-- graphics/ --> PNG and SVG files that can be referenced from anywhere in the Asset pack
    |                 Files can be organised with subdirectories
    |
    +-- sounds/ --> OGG files that can be reference from anywhere in the Asset pack
    |               Files can be organised with subdirectories
    |
    +-- Abilities/ ---> *.gd files registering new ability keyword functions
    |
    +-- Equipment/ --> Various Items and Armaments
    |   |
    |   +-- item_name.yaml
    |   |
    |   +-- ...
    |
    +-- Stories/
    |   |
    |   +-- Campaigns/
    |   |   |
    |   |   +-- map_name/
    |   |   |   |
    |   |   |   +-- Map.yaml
    |   |   |   |
    |   |   |   +-- generators/ --> *.gd scripts that return map definitions
    |   |   |
    |   |   +-- ...
    |   |
    |   +-- Dialogues
    |       |
    |       +-- dialogue_name.dialogue --> A Ren'Py-esque file type for telling a story
    |       |
    |       +-- ...
    |
    +-- Units/
        |
        +-- unit_name.yaml --> Definition of unit, file name is arbitrary
        |
        +-- ...
```

## Loading, Activating and Deactivating Assets

Ideally Asset Packs should be installable by dragging an Asset Pack file to the user's `$HOME/OpenHero/AssetPacks/` folder. Upon launching, a Player can choose what Asset Packs to include during gameplay.

If a Player obtains Units from an Asset Pack they then deactivate, these Units are hidden from the Roster, but are not removed. Re-activating the Asset Pack restore the Units to the Roster during subsequent sessions.
