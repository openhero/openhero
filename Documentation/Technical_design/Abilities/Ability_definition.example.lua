--[[
Ability defintion.

Units and weapons can override some of the configurations like value and tov
--]]

local custom_ability = {
    id = "org.myname:ability:heal"
    value = 5,
    tov = 100,
    display = {
        name = "Heal",
        description = "Heal one ally for %i HP",
        icon = "icon_heal.png"
    }
}
