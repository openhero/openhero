# Summoning

To obtain a Unit, the player must pay Ether Points into a gatcha roulette. They are then delivered one Unit chosen at random.

The pool from which to choose is built up of all the loaded units. Each Unit's Rarity points give it a higher chance of being selected during the roulette.

* a Rarity of 10 makes the Unit a common type
* a Rarity of 1 makes the Unit extremely rare
* a Rarity of 0 means it will never come out in a roulette
    * this is for Units designed solely to be enemies

## Suggested implementation

* Determine an "Entry" to be a `int,string` pair representing a chance value and a reference to a Unit
* Establish an array to hold "Entry" elements
* For each Unit with Rarity `R`
    * Find the previous Entry value (if none, use 0) as `X`
    * Add an Entry associating the Unit string with `Y` defined as `Y = X + R`
* Run the roulette to find an integer `P` between `1` and the last `Y` value assigned
* Binary-search the array until `prev(Y) <= P < next(y)` 
* Select Unit associated with the lower `Y` Entry

## Cost

A Player can pay 10 Ether Points to run the normal roulette. A Player can pay more Ether Points on top of the base cost to restrict the Rarity rating, at an increasingly expensive cost (`5 * n * n / 2` where n is how for to lower the cap, until 9)

The player selects the rarity level to cap at, not the number of Ether Points to attempt to pay. To only play amongst Rarity 1 Units, the Player can thus pay 202 Ether Points to obtain a character.

* Restrict 1-9 :  pay 10 + 2 = 12
* Restrict 1-8 :  pay 10 + 10 = 20
* Restrict to 1 : pay 10 + 202 = 212
    * To force-summon one Rarity-1 Unit, Player must win 212 battles without spending any EP
    * This feels about right for someone who wants to "bypass" the gatcha

```python
def taxes(basetax, adjuster):
 prev = 0
 for i in range(9):
  i = i+1
  tax = basetax*i*i/adjuster
  print("# %i -> %i (+%i)" % (i, tax, tax-prev) )
  prev = tax

taxes(5, 2)
# 1 -> 2 (+2)
# 2 -> 10 (+8)
# 3 -> 22 (+12)
# 4 -> 40 (+18)
# 5 -> 62 (+22)
# 6 -> 90 (+28)
# 7 -> 122 (+32)
# 8 -> 160 (+38)
# 9 -> 202 (+42)
```
