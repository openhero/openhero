# Asset Protection

By default, OpenHero stores its media assets on-disk as separate files from the application itself. However, some game makers may have agreements with their asset producers to not distribute these freely, and want to ensure players and custom apps cannot simply extract this information. Mechanisms for protection against asset leak should be standard in the base code, without necessarily providing the actual encryption/decryption mechanisms themselves.

The standard data-saving library needs to call a `CryptFile.encrypt(key,data)` before writing data to disk, and a `CryptFile.decrypt(key,data)` upon reading disk data. Responsibility is passed on to the game producer to provide the implementation of `CryptFile`, the default implementations simply return the data itself.
