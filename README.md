# OpenHero : An open source turn-based game engine

OpenHero is a game engine for turn-based combat games, designed with the intent of being heavily themable, cross-platform, extensible and lightweight.

Intended features include:

* MIT Expat License - the engine is licensed as free software, but you can choose whatever license you want for your own game
* Built with the Love2D framework
* Flexible combat unit definitions for the server to define custom heroes
* Themable backdrops and music for all areas of the game
* Campaigns and mini-maps for story-driven games
* Pluggable AI definitions to determine custom bots for single-player modes
