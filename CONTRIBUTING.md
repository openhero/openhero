# Contributing

Thank you for considering contributing to OpenHero. The following document is still under construction, so please check back during contribution.

Contributors are asked to note the following:

* The maintainer reserves the final right as to what is merged into the project and what must be left out
    * The current maintainer of this instance is Tai Kedzierski
* Contributors are invited to add their names to the `contributions/Contributors.md` file with the year of their first contribution. It's the club-membership roster, of sorts `:-)`

## Merge requests

When filing a merge request, please ensure

* Merge request is made to the `dev` branch, NOT `master`
* You are only changing the bare minumum for your contribution to be meaningful (don't submit multiple feature changes in a single Merge Request)
* Your commits are squashed (one commit per Merge Request, even if you used multiple commits during your development)
